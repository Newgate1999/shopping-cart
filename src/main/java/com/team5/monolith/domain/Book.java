package com.team5.monolith.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Book.
 */
@Entity
@Table(name = "book")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price_entry")
    private Float priceEntry;

    @Column(name = "date_of_etry")
    private Instant dateOfEtry;

    @Column(name = "author")
    private String author;

    @Column(name = "bar_code")
    private String barCode;

    @OneToMany(mappedBy = "book")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Item> items = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Book name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPriceEntry() {
        return priceEntry;
    }

    public Book priceEntry(Float priceEntry) {
        this.priceEntry = priceEntry;
        return this;
    }

    public void setPriceEntry(Float priceEntry) {
        this.priceEntry = priceEntry;
    }

    public Instant getDateOfEtry() {
        return dateOfEtry;
    }

    public Book dateOfEtry(Instant dateOfEtry) {
        this.dateOfEtry = dateOfEtry;
        return this;
    }

    public void setDateOfEtry(Instant dateOfEtry) {
        this.dateOfEtry = dateOfEtry;
    }

    public String getAuthor() {
        return author;
    }

    public Book author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBarCode() {
        return barCode;
    }

    public Book barCode(String barCode) {
        this.barCode = barCode;
        return this;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Set<Item> getItems() {
        return items;
    }

    public Book items(Set<Item> items) {
        this.items = items;
        return this;
    }

    public Book addItem(Item item) {
        this.items.add(item);
        item.setBook(this);
        return this;
    }

    public Book removeItem(Item item) {
        this.items.remove(item);
        item.setBook(null);
        return this;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Book)) {
            return false;
        }
        return id != null && id.equals(((Book) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Book{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", priceEntry=" + getPriceEntry() +
            ", dateOfEtry='" + getDateOfEtry() + "'" +
            ", author='" + getAuthor() + "'" +
            ", barCode='" + getBarCode() + "'" +
            "}";
    }
}
