package com.team5.monolith.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Shipping.
 */
@Entity
@Table(name = "shipping")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Shipping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "free")
    private Float free;

    @Column(name = "receive_date")
    private String receiveDate;

    @Column(name = "shipping_status")
    private String shippingStatus;

    @Column(name = "shipping_type")
    private String shippingType;

    @OneToOne
    @JoinColumn(unique = true)
    private Order order;

    @OneToOne
    @JoinColumn(unique = true)
    private Payment payment;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getFree() {
        return free;
    }

    public Shipping free(Float free) {
        this.free = free;
        return this;
    }

    public void setFree(Float free) {
        this.free = free;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public Shipping receiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
        return this;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public Shipping shippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
        return this;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public String getShippingType() {
        return shippingType;
    }

    public Shipping shippingType(String shippingType) {
        this.shippingType = shippingType;
        return this;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public Order getOrder() {
        return order;
    }

    public Shipping order(Order order) {
        this.order = order;
        return this;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Payment getPayment() {
        return payment;
    }

    public Shipping payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shipping)) {
            return false;
        }
        return id != null && id.equals(((Shipping) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Shipping{" +
            "id=" + getId() +
            ", free=" + getFree() +
            ", receiveDate='" + getReceiveDate() + "'" +
            ", shippingStatus='" + getShippingStatus() + "'" +
            ", shippingType='" + getShippingType() + "'" +
            "}";
    }
}
