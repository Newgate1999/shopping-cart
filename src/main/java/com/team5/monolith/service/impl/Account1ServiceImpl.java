package com.team5.monolith.service.impl;

import com.team5.monolith.service.Account1Service;
import com.team5.monolith.domain.Account1;
import com.team5.monolith.repository.Account1Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Account1}.
 */
@Service
@Transactional
public class Account1ServiceImpl implements Account1Service {

    private final Logger log = LoggerFactory.getLogger(Account1ServiceImpl.class);

    private final Account1Repository account1Repository;

    public Account1ServiceImpl(Account1Repository account1Repository) {
        this.account1Repository = account1Repository;
    }

    @Override
    public Account1 save(Account1 account1) {
        log.debug("Request to save Account1 : {}", account1);
        return account1Repository.save(account1);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Account1> findAll() {
        log.debug("Request to get all Account1s");
        return account1Repository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Account1> findOne(Long id) {
        log.debug("Request to get Account1 : {}", id);
        return account1Repository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Account1 : {}", id);
        account1Repository.deleteById(id);
    }
}
