package com.team5.monolith.service;

import com.team5.monolith.domain.Account1;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Account1}.
 */
public interface Account1Service {

    /**
     * Save a account1.
     *
     * @param account1 the entity to save.
     * @return the persisted entity.
     */
    Account1 save(Account1 account1);

    /**
     * Get all the account1s.
     *
     * @return the list of entities.
     */
    List<Account1> findAll();


    /**
     * Get the "id" account1.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Account1> findOne(Long id);

    /**
     * Delete the "id" account1.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
