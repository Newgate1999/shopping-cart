package com.team5.monolith.web.rest;

import com.team5.monolith.domain.FullName;
import com.team5.monolith.repository.FullNameRepository;
import com.team5.monolith.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.team5.monolith.domain.FullName}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FullNameResource {

    private final Logger log = LoggerFactory.getLogger(FullNameResource.class);

    private static final String ENTITY_NAME = "fullName";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FullNameRepository fullNameRepository;

    public FullNameResource(FullNameRepository fullNameRepository) {
        this.fullNameRepository = fullNameRepository;
    }

    /**
     * {@code POST  /full-names} : Create a new fullName.
     *
     * @param fullName the fullName to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fullName, or with status {@code 400 (Bad Request)} if the fullName has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/full-names")
    public ResponseEntity<FullName> createFullName(@RequestBody FullName fullName) throws URISyntaxException {
        log.debug("REST request to save FullName : {}", fullName);
        if (fullName.getId() != null) {
            throw new BadRequestAlertException("A new fullName cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FullName result = fullNameRepository.save(fullName);
        return ResponseEntity.created(new URI("/api/full-names/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /full-names} : Updates an existing fullName.
     *
     * @param fullName the fullName to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fullName,
     * or with status {@code 400 (Bad Request)} if the fullName is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fullName couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/full-names")
    public ResponseEntity<FullName> updateFullName(@RequestBody FullName fullName) throws URISyntaxException {
        log.debug("REST request to update FullName : {}", fullName);
        if (fullName.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FullName result = fullNameRepository.save(fullName);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fullName.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /full-names} : get all the fullNames.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fullNames in body.
     */
    @GetMapping("/full-names")
    public List<FullName> getAllFullNames() {
        log.debug("REST request to get all FullNames");
        return fullNameRepository.findAll();
    }

    /**
     * {@code GET  /full-names/:id} : get the "id" fullName.
     *
     * @param id the id of the fullName to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fullName, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/full-names/{id}")
    public ResponseEntity<FullName> getFullName(@PathVariable Long id) {
        log.debug("REST request to get FullName : {}", id);
        Optional<FullName> fullName = fullNameRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fullName);
    }

    /**
     * {@code DELETE  /full-names/:id} : delete the "id" fullName.
     *
     * @param id the id of the fullName to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/full-names/{id}")
    public ResponseEntity<Void> deleteFullName(@PathVariable Long id) {
        log.debug("REST request to delete FullName : {}", id);
        fullNameRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
