package com.team5.monolith.web.rest;

import com.team5.monolith.domain.Account1;
import com.team5.monolith.service.Account1Service;
import com.team5.monolith.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.team5.monolith.domain.Account1}.
 */
@RestController
@RequestMapping("/api")
public class Account1Resource {

    private final Logger log = LoggerFactory.getLogger(Account1Resource.class);

    private static final String ENTITY_NAME = "account1";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Account1Service account1Service;

    public Account1Resource(Account1Service account1Service) {
        this.account1Service = account1Service;
    }

    /**
     * {@code POST  /account-1-s} : Create a new account1.
     *
     * @param account1 the account1 to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new account1, or with status {@code 400 (Bad Request)} if the account1 has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/account-1-s")
    public ResponseEntity<Account1> createAccount1(@Valid @RequestBody Account1 account1) throws URISyntaxException {
        log.debug("REST request to save Account1 : {}", account1);
        if (account1.getId() != null) {
            throw new BadRequestAlertException("A new account1 cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Account1 result = account1Service.save(account1);
        return ResponseEntity.created(new URI("/api/account-1-s/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /account-1-s} : Updates an existing account1.
     *
     * @param account1 the account1 to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated account1,
     * or with status {@code 400 (Bad Request)} if the account1 is not valid,
     * or with status {@code 500 (Internal Server Error)} if the account1 couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/account-1-s")
    public ResponseEntity<Account1> updateAccount1(@Valid @RequestBody Account1 account1) throws URISyntaxException {
        log.debug("REST request to update Account1 : {}", account1);
        if (account1.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Account1 result = account1Service.save(account1);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, account1.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /account-1-s} : get all the account1s.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of account1s in body.
     */
    @GetMapping("/account-1-s")
    public List<Account1> getAllAccount1s() {
        log.debug("REST request to get all Account1s");
        return account1Service.findAll();
    }

    /**
     * {@code GET  /account-1-s/:id} : get the "id" account1.
     *
     * @param id the id of the account1 to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the account1, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/account-1-s/{id}")
    public ResponseEntity<Account1> getAccount1(@PathVariable Long id) {
        log.debug("REST request to get Account1 : {}", id);
        Optional<Account1> account1 = account1Service.findOne(id);
        return ResponseUtil.wrapOrNotFound(account1);
    }

    /**
     * {@code DELETE  /account-1-s/:id} : delete the "id" account1.
     *
     * @param id the id of the account1 to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/account-1-s/{id}")
    public ResponseEntity<Void> deleteAccount1(@PathVariable Long id) {
        log.debug("REST request to delete Account1 : {}", id);
        account1Service.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
