package com.team5.monolith.repository;

import com.team5.monolith.domain.FullName;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FullName entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FullNameRepository extends JpaRepository<FullName, Long> {
}
