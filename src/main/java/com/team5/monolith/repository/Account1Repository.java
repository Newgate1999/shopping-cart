package com.team5.monolith.repository;

import com.team5.monolith.domain.Account1;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Account1 entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Account1Repository extends JpaRepository<Account1, Long> {
}
