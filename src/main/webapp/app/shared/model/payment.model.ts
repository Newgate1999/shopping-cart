import { Moment } from 'moment';
import { IOrder } from 'app/shared/model/order.model';

export interface IPayment {
  id?: number;
  total?: number;
  paymentDate?: Moment;
  order?: IOrder;
}

export class Payment implements IPayment {
  constructor(public id?: number, public total?: number, public paymentDate?: Moment, public order?: IOrder) {}
}
