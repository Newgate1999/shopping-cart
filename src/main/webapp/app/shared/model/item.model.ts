import { ICart } from 'app/shared/model/cart.model';
import { IOrder } from 'app/shared/model/order.model';
import { IBook } from 'app/shared/model/book.model';

export interface IItem {
  id?: number;
  discount?: number;
  price?: number;
  quantity?: number;
  cart?: ICart;
  order?: IOrder;
  book?: IBook;
}

export class Item implements IItem {
  constructor(
    public id?: number,
    public discount?: number,
    public price?: number,
    public quantity?: number,
    public cart?: ICart,
    public order?: IOrder,
    public book?: IBook
  ) {}
}
