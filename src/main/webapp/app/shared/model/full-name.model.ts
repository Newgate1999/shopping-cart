export interface IFullName {
  id?: number;
  firstName?: string;
  lastName?: string;
  midName?: string;
}

export class FullName implements IFullName {
  constructor(public id?: number, public firstName?: string, public lastName?: string, public midName?: string) {}
}
