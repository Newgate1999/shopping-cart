import { IOrder } from 'app/shared/model/order.model';
import { IPayment } from 'app/shared/model/payment.model';

export interface IShipping {
  id?: number;
  free?: number;
  receiveDate?: string;
  shippingStatus?: string;
  shippingType?: string;
  order?: IOrder;
  payment?: IPayment;
}

export class Shipping implements IShipping {
  constructor(
    public id?: number,
    public free?: number,
    public receiveDate?: string,
    public shippingStatus?: string,
    public shippingType?: string,
    public order?: IOrder,
    public payment?: IPayment
  ) {}
}
