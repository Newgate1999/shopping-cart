import { Moment } from 'moment';
import { IItem } from 'app/shared/model/item.model';

export interface IBook {
  id?: number;
  name?: string;
  priceEntry?: number;
  dateOfEtry?: Moment;
  author?: string;
  barCode?: string;
  items?: IItem[];
}

export class Book implements IBook {
  constructor(
    public id?: number,
    public name?: string,
    public priceEntry?: number,
    public dateOfEtry?: Moment,
    public author?: string,
    public barCode?: string,
    public items?: IItem[]
  ) {}
}
