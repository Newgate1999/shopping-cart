export interface IAccount1 {
  id?: number;
  username?: string;
  password?: string;
}

export class Account1 implements IAccount1 {
  constructor(public id?: number, public username?: string, public password?: string) {}
}
