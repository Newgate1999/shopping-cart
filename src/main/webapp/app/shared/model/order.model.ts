import { Moment } from 'moment';
import { IItem } from 'app/shared/model/item.model';
import { ICustomer } from 'app/shared/model/customer.model';

export interface IOrder {
  id?: number;
  createdDate?: Moment;
  total?: number;
  code?: string;
  items?: IItem[];
  customer?: ICustomer;
}

export class Order implements IOrder {
  constructor(
    public id?: number,
    public createdDate?: Moment,
    public total?: number,
    public code?: string,
    public items?: IItem[],
    public customer?: ICustomer
  ) {}
}
