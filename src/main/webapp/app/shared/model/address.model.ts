export interface IAddress {
  id?: number;
  numberHouse?: string;
  street?: string;
  district?: string;
  city?: string;
}

export class Address implements IAddress {
  constructor(public id?: number, public numberHouse?: string, public street?: string, public district?: string, public city?: string) {}
}
