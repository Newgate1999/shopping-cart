import { Moment } from 'moment';
import { IAccount1 } from 'app/shared/model/account-1.model';
import { IFullName } from 'app/shared/model/full-name.model';
import { IAddress } from 'app/shared/model/address.model';
import { IOrder } from 'app/shared/model/order.model';
import { ICart } from 'app/shared/model/cart.model';

export interface ICustomer {
  id?: number;
  email?: string;
  phoneNumber?: string;
  dateOfBirth?: Moment;
  sex?: number;
  account?: IAccount1;
  fullName?: IFullName;
  address?: IAddress;
  orders?: IOrder[];
  carts?: ICart[];
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public email?: string,
    public phoneNumber?: string,
    public dateOfBirth?: Moment,
    public sex?: number,
    public account?: IAccount1,
    public fullName?: IFullName,
    public address?: IAddress,
    public orders?: IOrder[],
    public carts?: ICart[]
  ) {}
}
