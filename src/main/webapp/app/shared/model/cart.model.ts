import { IItem } from 'app/shared/model/item.model';
import { ICustomer } from 'app/shared/model/customer.model';

export interface ICart {
  id?: number;
  total?: number;
  quantity?: number;
  items?: IItem[];
  customer?: ICustomer;
}

export class Cart implements ICart {
  constructor(public id?: number, public total?: number, public quantity?: number, public items?: IItem[], public customer?: ICustomer) {}
}
