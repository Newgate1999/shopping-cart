import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingSharedModule } from 'app/shared/shared.module';
import { FullNameComponent } from './full-name.component';
import { FullNameDetailComponent } from './full-name-detail.component';
import { FullNameUpdateComponent } from './full-name-update.component';
import { FullNameDeleteDialogComponent } from './full-name-delete-dialog.component';
import { fullNameRoute } from './full-name.route';

@NgModule({
  imports: [ShoppingSharedModule, RouterModule.forChild(fullNameRoute)],
  declarations: [FullNameComponent, FullNameDetailComponent, FullNameUpdateComponent, FullNameDeleteDialogComponent],
  entryComponents: [FullNameDeleteDialogComponent],
})
export class ShoppingFullNameModule {}
