import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFullName } from 'app/shared/model/full-name.model';
import { FullNameService } from './full-name.service';

@Component({
  templateUrl: './full-name-delete-dialog.component.html',
})
export class FullNameDeleteDialogComponent {
  fullName?: IFullName;

  constructor(protected fullNameService: FullNameService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fullNameService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fullNameListModification');
      this.activeModal.close();
    });
  }
}
