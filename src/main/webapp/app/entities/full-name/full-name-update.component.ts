import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFullName, FullName } from 'app/shared/model/full-name.model';
import { FullNameService } from './full-name.service';

@Component({
  selector: 'jhi-full-name-update',
  templateUrl: './full-name-update.component.html',
})
export class FullNameUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: [],
    midName: [],
  });

  constructor(protected fullNameService: FullNameService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fullName }) => {
      this.updateForm(fullName);
    });
  }

  updateForm(fullName: IFullName): void {
    this.editForm.patchValue({
      id: fullName.id,
      firstName: fullName.firstName,
      lastName: fullName.lastName,
      midName: fullName.midName,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fullName = this.createFromForm();
    if (fullName.id !== undefined) {
      this.subscribeToSaveResponse(this.fullNameService.update(fullName));
    } else {
      this.subscribeToSaveResponse(this.fullNameService.create(fullName));
    }
  }

  private createFromForm(): IFullName {
    return {
      ...new FullName(),
      id: this.editForm.get(['id'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      midName: this.editForm.get(['midName'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFullName>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
