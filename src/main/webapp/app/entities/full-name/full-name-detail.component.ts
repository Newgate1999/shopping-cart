import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFullName } from 'app/shared/model/full-name.model';

@Component({
  selector: 'jhi-full-name-detail',
  templateUrl: './full-name-detail.component.html',
})
export class FullNameDetailComponent implements OnInit {
  fullName: IFullName | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fullName }) => (this.fullName = fullName));
  }

  previousState(): void {
    window.history.back();
  }
}
