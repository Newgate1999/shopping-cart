import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFullName, FullName } from 'app/shared/model/full-name.model';
import { FullNameService } from './full-name.service';
import { FullNameComponent } from './full-name.component';
import { FullNameDetailComponent } from './full-name-detail.component';
import { FullNameUpdateComponent } from './full-name-update.component';

@Injectable({ providedIn: 'root' })
export class FullNameResolve implements Resolve<IFullName> {
  constructor(private service: FullNameService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFullName> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fullName: HttpResponse<FullName>) => {
          if (fullName.body) {
            return of(fullName.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FullName());
  }
}

export const fullNameRoute: Routes = [
  {
    path: '',
    component: FullNameComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'FullNames',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FullNameDetailComponent,
    resolve: {
      fullName: FullNameResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'FullNames',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FullNameUpdateComponent,
    resolve: {
      fullName: FullNameResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'FullNames',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FullNameUpdateComponent,
    resolve: {
      fullName: FullNameResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'FullNames',
    },
    canActivate: [UserRouteAccessService],
  },
];
