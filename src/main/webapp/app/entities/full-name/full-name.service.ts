import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFullName } from 'app/shared/model/full-name.model';

type EntityResponseType = HttpResponse<IFullName>;
type EntityArrayResponseType = HttpResponse<IFullName[]>;

@Injectable({ providedIn: 'root' })
export class FullNameService {
  public resourceUrl = SERVER_API_URL + 'api/full-names';

  constructor(protected http: HttpClient) {}

  create(fullName: IFullName): Observable<EntityResponseType> {
    return this.http.post<IFullName>(this.resourceUrl, fullName, { observe: 'response' });
  }

  update(fullName: IFullName): Observable<EntityResponseType> {
    return this.http.put<IFullName>(this.resourceUrl, fullName, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFullName>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFullName[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
