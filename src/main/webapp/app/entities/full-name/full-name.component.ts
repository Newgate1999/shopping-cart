import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFullName } from 'app/shared/model/full-name.model';
import { FullNameService } from './full-name.service';
import { FullNameDeleteDialogComponent } from './full-name-delete-dialog.component';

@Component({
  selector: 'jhi-full-name',
  templateUrl: './full-name.component.html',
})
export class FullNameComponent implements OnInit, OnDestroy {
  fullNames?: IFullName[];
  eventSubscriber?: Subscription;

  constructor(protected fullNameService: FullNameService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fullNameService.query().subscribe((res: HttpResponse<IFullName[]>) => (this.fullNames = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFullNames();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFullName): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFullNames(): void {
    this.eventSubscriber = this.eventManager.subscribe('fullNameListModification', () => this.loadAll());
  }

  delete(fullName: IFullName): void {
    const modalRef = this.modalService.open(FullNameDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fullName = fullName;
  }
}
