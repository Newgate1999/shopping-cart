import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.ShoppingCustomerModule),
      },
      {
        path: 'full-name',
        loadChildren: () => import('./full-name/full-name.module').then(m => m.ShoppingFullNameModule),
      },
      {
        path: 'address',
        loadChildren: () => import('./address/address.module').then(m => m.ShoppingAddressModule),
      },
      {
        path: 'account-1',
        loadChildren: () => import('./account-1/account-1.module').then(m => m.ShoppingAccount1Module),
      },
      {
        path: 'cart',
        loadChildren: () => import('./cart/cart.module').then(m => m.ShoppingCartModule),
      },
      {
        path: 'item',
        loadChildren: () => import('./item/item.module').then(m => m.ShoppingItemModule),
      },
      {
        path: 'order',
        loadChildren: () => import('./order/order.module').then(m => m.ShoppingOrderModule),
      },
      {
        path: 'book',
        loadChildren: () => import('./book/book.module').then(m => m.ShoppingBookModule),
      },
      {
        path: 'payment',
        loadChildren: () => import('./payment/payment.module').then(m => m.ShoppingPaymentModule),
      },
      {
        path: 'shipping',
        loadChildren: () => import('./shipping/shipping.module').then(m => m.ShoppingShippingModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ShoppingEntityModule {}
