import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAccount1 } from 'app/shared/model/account-1.model';

@Component({
  selector: 'jhi-account-1-detail',
  templateUrl: './account-1-detail.component.html',
})
export class Account1DetailComponent implements OnInit {
  account1: IAccount1 | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ account1 }) => (this.account1 = account1));
  }

  previousState(): void {
    window.history.back();
  }
}
