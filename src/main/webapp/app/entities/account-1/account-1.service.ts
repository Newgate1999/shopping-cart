import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAccount1 } from 'app/shared/model/account-1.model';

type EntityResponseType = HttpResponse<IAccount1>;
type EntityArrayResponseType = HttpResponse<IAccount1[]>;

@Injectable({ providedIn: 'root' })
export class Account1Service {
  public resourceUrl = SERVER_API_URL + 'api/account-1-s';

  constructor(protected http: HttpClient) {}

  create(account1: IAccount1): Observable<EntityResponseType> {
    return this.http.post<IAccount1>(this.resourceUrl, account1, { observe: 'response' });
  }

  update(account1: IAccount1): Observable<EntityResponseType> {
    return this.http.put<IAccount1>(this.resourceUrl, account1, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAccount1>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccount1[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
