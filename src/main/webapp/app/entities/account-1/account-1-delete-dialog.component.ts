import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAccount1 } from 'app/shared/model/account-1.model';
import { Account1Service } from './account-1.service';

@Component({
  templateUrl: './account-1-delete-dialog.component.html',
})
export class Account1DeleteDialogComponent {
  account1?: IAccount1;

  constructor(protected account1Service: Account1Service, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.account1Service.delete(id).subscribe(() => {
      this.eventManager.broadcast('account1ListModification');
      this.activeModal.close();
    });
  }
}
