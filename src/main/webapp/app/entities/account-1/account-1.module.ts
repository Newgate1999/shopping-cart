import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingSharedModule } from 'app/shared/shared.module';
import { Account1Component } from './account-1.component';
import { Account1DetailComponent } from './account-1-detail.component';
import { Account1UpdateComponent } from './account-1-update.component';
import { Account1DeleteDialogComponent } from './account-1-delete-dialog.component';
import { account1Route } from './account-1.route';

@NgModule({
  imports: [ShoppingSharedModule, RouterModule.forChild(account1Route)],
  declarations: [Account1Component, Account1DetailComponent, Account1UpdateComponent, Account1DeleteDialogComponent],
  entryComponents: [Account1DeleteDialogComponent],
})
export class ShoppingAccount1Module {}
