import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccount1 } from 'app/shared/model/account-1.model';
import { Account1Service } from './account-1.service';
import { Account1DeleteDialogComponent } from './account-1-delete-dialog.component';

@Component({
  selector: 'jhi-account-1',
  templateUrl: './account-1.component.html',
})
export class Account1Component implements OnInit, OnDestroy {
  account1s?: IAccount1[];
  eventSubscriber?: Subscription;

  constructor(protected account1Service: Account1Service, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.account1Service.query().subscribe((res: HttpResponse<IAccount1[]>) => (this.account1s = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAccount1s();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAccount1): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAccount1s(): void {
    this.eventSubscriber = this.eventManager.subscribe('account1ListModification', () => this.loadAll());
  }

  delete(account1: IAccount1): void {
    const modalRef = this.modalService.open(Account1DeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.account1 = account1;
  }
}
