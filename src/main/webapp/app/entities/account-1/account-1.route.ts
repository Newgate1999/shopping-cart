import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAccount1, Account1 } from 'app/shared/model/account-1.model';
import { Account1Service } from './account-1.service';
import { Account1Component } from './account-1.component';
import { Account1DetailComponent } from './account-1-detail.component';
import { Account1UpdateComponent } from './account-1-update.component';

@Injectable({ providedIn: 'root' })
export class Account1Resolve implements Resolve<IAccount1> {
  constructor(private service: Account1Service, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAccount1> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((account1: HttpResponse<Account1>) => {
          if (account1.body) {
            return of(account1.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Account1());
  }
}

export const account1Route: Routes = [
  {
    path: '',
    component: Account1Component,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Account1s',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: Account1DetailComponent,
    resolve: {
      account1: Account1Resolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Account1s',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: Account1UpdateComponent,
    resolve: {
      account1: Account1Resolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Account1s',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: Account1UpdateComponent,
    resolve: {
      account1: Account1Resolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Account1s',
    },
    canActivate: [UserRouteAccessService],
  },
];
