import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAccount1, Account1 } from 'app/shared/model/account-1.model';
import { Account1Service } from './account-1.service';

@Component({
  selector: 'jhi-account-1-update',
  templateUrl: './account-1-update.component.html',
})
export class Account1UpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    username: [null, [Validators.required]],
    password: [null, [Validators.required]],
  });

  constructor(protected account1Service: Account1Service, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ account1 }) => {
      this.updateForm(account1);
    });
  }

  updateForm(account1: IAccount1): void {
    this.editForm.patchValue({
      id: account1.id,
      username: account1.username,
      password: account1.password,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const account1 = this.createFromForm();
    if (account1.id !== undefined) {
      this.subscribeToSaveResponse(this.account1Service.update(account1));
    } else {
      this.subscribeToSaveResponse(this.account1Service.create(account1));
    }
  }

  private createFromForm(): IAccount1 {
    return {
      ...new Account1(),
      id: this.editForm.get(['id'])!.value,
      username: this.editForm.get(['username'])!.value,
      password: this.editForm.get(['password'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccount1>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
