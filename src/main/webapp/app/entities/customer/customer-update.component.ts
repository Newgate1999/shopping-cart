import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICustomer, Customer } from 'app/shared/model/customer.model';
import { CustomerService } from './customer.service';
import { IAccount1 } from 'app/shared/model/account-1.model';
import { Account1Service } from 'app/entities/account-1/account-1.service';
import { IFullName } from 'app/shared/model/full-name.model';
import { FullNameService } from 'app/entities/full-name/full-name.service';
import { IAddress } from 'app/shared/model/address.model';
import { AddressService } from 'app/entities/address/address.service';

type SelectableEntity = IAccount1 | IFullName | IAddress;

@Component({
  selector: 'jhi-customer-update',
  templateUrl: './customer-update.component.html',
})
export class CustomerUpdateComponent implements OnInit {
  isSaving = false;
  accounts: IAccount1[] = [];
  fullnames: IFullName[] = [];
  addresses: IAddress[] = [];

  editForm = this.fb.group({
    id: [],
    email: [],
    phoneNumber: [],
    dateOfBirth: [],
    sex: [],
    account: [],
    fullName: [],
    address: [],
  });

  constructor(
    protected customerService: CustomerService,
    protected account1Service: Account1Service,
    protected fullNameService: FullNameService,
    protected addressService: AddressService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customer }) => {
      if (!customer.id) {
        const today = moment().startOf('day');
        customer.dateOfBirth = today;
      }

      this.updateForm(customer);

      this.account1Service
        .query({ filter: 'customer-is-null' })
        .pipe(
          map((res: HttpResponse<IAccount1[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAccount1[]) => {
          if (!customer.account || !customer.account.id) {
            this.accounts = resBody;
          } else {
            this.account1Service
              .find(customer.account.id)
              .pipe(
                map((subRes: HttpResponse<IAccount1>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAccount1[]) => (this.accounts = concatRes));
          }
        });

      this.fullNameService
        .query({ filter: 'customer-is-null' })
        .pipe(
          map((res: HttpResponse<IFullName[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFullName[]) => {
          if (!customer.fullName || !customer.fullName.id) {
            this.fullnames = resBody;
          } else {
            this.fullNameService
              .find(customer.fullName.id)
              .pipe(
                map((subRes: HttpResponse<IFullName>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFullName[]) => (this.fullnames = concatRes));
          }
        });

      this.addressService
        .query({ filter: 'customer-is-null' })
        .pipe(
          map((res: HttpResponse<IAddress[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAddress[]) => {
          if (!customer.address || !customer.address.id) {
            this.addresses = resBody;
          } else {
            this.addressService
              .find(customer.address.id)
              .pipe(
                map((subRes: HttpResponse<IAddress>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAddress[]) => (this.addresses = concatRes));
          }
        });
    });
  }

  updateForm(customer: ICustomer): void {
    this.editForm.patchValue({
      id: customer.id,
      email: customer.email,
      phoneNumber: customer.phoneNumber,
      dateOfBirth: customer.dateOfBirth ? customer.dateOfBirth.format(DATE_TIME_FORMAT) : null,
      sex: customer.sex,
      account: customer.account,
      fullName: customer.fullName,
      address: customer.address,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customer = this.createFromForm();
    if (customer.id !== undefined) {
      this.subscribeToSaveResponse(this.customerService.update(customer));
    } else {
      this.subscribeToSaveResponse(this.customerService.create(customer));
    }
  }

  private createFromForm(): ICustomer {
    return {
      ...new Customer(),
      id: this.editForm.get(['id'])!.value,
      email: this.editForm.get(['email'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      dateOfBirth: this.editForm.get(['dateOfBirth'])!.value
        ? moment(this.editForm.get(['dateOfBirth'])!.value, DATE_TIME_FORMAT)
        : undefined,
      sex: this.editForm.get(['sex'])!.value,
      account: this.editForm.get(['account'])!.value,
      fullName: this.editForm.get(['fullName'])!.value,
      address: this.editForm.get(['address'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
