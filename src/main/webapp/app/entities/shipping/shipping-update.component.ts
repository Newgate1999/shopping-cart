import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IShipping, Shipping } from 'app/shared/model/shipping.model';
import { ShippingService } from './shipping.service';
import { IOrder } from 'app/shared/model/order.model';
import { OrderService } from 'app/entities/order/order.service';
import { IPayment } from 'app/shared/model/payment.model';
import { PaymentService } from 'app/entities/payment/payment.service';

type SelectableEntity = IOrder | IPayment;

@Component({
  selector: 'jhi-shipping-update',
  templateUrl: './shipping-update.component.html',
})
export class ShippingUpdateComponent implements OnInit {
  isSaving = false;
  orders: IOrder[] = [];
  payments: IPayment[] = [];

  editForm = this.fb.group({
    id: [],
    free: [],
    receiveDate: [],
    shippingStatus: [],
    shippingType: [],
    order: [],
    payment: [],
  });

  constructor(
    protected shippingService: ShippingService,
    protected orderService: OrderService,
    protected paymentService: PaymentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shipping }) => {
      this.updateForm(shipping);

      this.orderService
        .query({ filter: 'shipping-is-null' })
        .pipe(
          map((res: HttpResponse<IOrder[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IOrder[]) => {
          if (!shipping.order || !shipping.order.id) {
            this.orders = resBody;
          } else {
            this.orderService
              .find(shipping.order.id)
              .pipe(
                map((subRes: HttpResponse<IOrder>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IOrder[]) => (this.orders = concatRes));
          }
        });

      this.paymentService
        .query({ filter: 'shipping-is-null' })
        .pipe(
          map((res: HttpResponse<IPayment[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IPayment[]) => {
          if (!shipping.payment || !shipping.payment.id) {
            this.payments = resBody;
          } else {
            this.paymentService
              .find(shipping.payment.id)
              .pipe(
                map((subRes: HttpResponse<IPayment>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IPayment[]) => (this.payments = concatRes));
          }
        });
    });
  }

  updateForm(shipping: IShipping): void {
    this.editForm.patchValue({
      id: shipping.id,
      free: shipping.free,
      receiveDate: shipping.receiveDate,
      shippingStatus: shipping.shippingStatus,
      shippingType: shipping.shippingType,
      order: shipping.order,
      payment: shipping.payment,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shipping = this.createFromForm();
    if (shipping.id !== undefined) {
      this.subscribeToSaveResponse(this.shippingService.update(shipping));
    } else {
      this.subscribeToSaveResponse(this.shippingService.create(shipping));
    }
  }

  private createFromForm(): IShipping {
    return {
      ...new Shipping(),
      id: this.editForm.get(['id'])!.value,
      free: this.editForm.get(['free'])!.value,
      receiveDate: this.editForm.get(['receiveDate'])!.value,
      shippingStatus: this.editForm.get(['shippingStatus'])!.value,
      shippingType: this.editForm.get(['shippingType'])!.value,
      order: this.editForm.get(['order'])!.value,
      payment: this.editForm.get(['payment'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShipping>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
