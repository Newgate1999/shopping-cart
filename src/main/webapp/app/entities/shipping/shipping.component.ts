import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShipping } from 'app/shared/model/shipping.model';
import { ShippingService } from './shipping.service';
import { ShippingDeleteDialogComponent } from './shipping-delete-dialog.component';

@Component({
  selector: 'jhi-shipping',
  templateUrl: './shipping.component.html',
})
export class ShippingComponent implements OnInit, OnDestroy {
  shippings?: IShipping[];
  eventSubscriber?: Subscription;

  constructor(protected shippingService: ShippingService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.shippingService.query().subscribe((res: HttpResponse<IShipping[]>) => (this.shippings = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShippings();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShipping): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShippings(): void {
    this.eventSubscriber = this.eventManager.subscribe('shippingListModification', () => this.loadAll());
  }

  delete(shipping: IShipping): void {
    const modalRef = this.modalService.open(ShippingDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shipping = shipping;
  }
}
