package com.team5.monolith;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.team5.monolith");

        noClasses()
            .that()
                .resideInAnyPackage("com.team5.monolith.service..")
            .or()
                .resideInAnyPackage("com.team5.monolith.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.team5.monolith.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
