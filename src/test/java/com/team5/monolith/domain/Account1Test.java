package com.team5.monolith.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.team5.monolith.web.rest.TestUtil;

public class Account1Test {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Account1.class);
        Account1 account11 = new Account1();
        account11.setId(1L);
        Account1 account12 = new Account1();
        account12.setId(account11.getId());
        assertThat(account11).isEqualTo(account12);
        account12.setId(2L);
        assertThat(account11).isNotEqualTo(account12);
        account11.setId(null);
        assertThat(account11).isNotEqualTo(account12);
    }
}
