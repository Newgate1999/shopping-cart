package com.team5.monolith.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.team5.monolith.web.rest.TestUtil;

public class FullNameTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FullName.class);
        FullName fullName1 = new FullName();
        fullName1.setId(1L);
        FullName fullName2 = new FullName();
        fullName2.setId(fullName1.getId());
        assertThat(fullName1).isEqualTo(fullName2);
        fullName2.setId(2L);
        assertThat(fullName1).isNotEqualTo(fullName2);
        fullName1.setId(null);
        assertThat(fullName1).isNotEqualTo(fullName2);
    }
}
