package com.team5.monolith.web.rest;

import com.team5.monolith.ShoppingApp;
import com.team5.monolith.domain.Account1;
import com.team5.monolith.repository.Account1Repository;
import com.team5.monolith.service.Account1Service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link Account1Resource} REST controller.
 */
@SpringBootTest(classes = ShoppingApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class Account1ResourceIT {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private Account1Repository account1Repository;

    @Autowired
    private Account1Service account1Service;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAccount1MockMvc;

    private Account1 account1;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Account1 createEntity(EntityManager em) {
        Account1 account1 = new Account1()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD);
        return account1;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Account1 createUpdatedEntity(EntityManager em) {
        Account1 account1 = new Account1()
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD);
        return account1;
    }

    @BeforeEach
    public void initTest() {
        account1 = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccount1() throws Exception {
        int databaseSizeBeforeCreate = account1Repository.findAll().size();
        // Create the Account1
        restAccount1MockMvc.perform(post("/api/account-1-s")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(account1)))
            .andExpect(status().isCreated());

        // Validate the Account1 in the database
        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeCreate + 1);
        Account1 testAccount1 = account1List.get(account1List.size() - 1);
        assertThat(testAccount1.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testAccount1.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createAccount1WithExistingId() throws Exception {
        int databaseSizeBeforeCreate = account1Repository.findAll().size();

        // Create the Account1 with an existing ID
        account1.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccount1MockMvc.perform(post("/api/account-1-s")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(account1)))
            .andExpect(status().isBadRequest());

        // Validate the Account1 in the database
        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = account1Repository.findAll().size();
        // set the field null
        account1.setUsername(null);

        // Create the Account1, which fails.


        restAccount1MockMvc.perform(post("/api/account-1-s")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(account1)))
            .andExpect(status().isBadRequest());

        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = account1Repository.findAll().size();
        // set the field null
        account1.setPassword(null);

        // Create the Account1, which fails.


        restAccount1MockMvc.perform(post("/api/account-1-s")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(account1)))
            .andExpect(status().isBadRequest());

        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAccount1s() throws Exception {
        // Initialize the database
        account1Repository.saveAndFlush(account1);

        // Get all the account1List
        restAccount1MockMvc.perform(get("/api/account-1-s?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(account1.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }
    
    @Test
    @Transactional
    public void getAccount1() throws Exception {
        // Initialize the database
        account1Repository.saveAndFlush(account1);

        // Get the account1
        restAccount1MockMvc.perform(get("/api/account-1-s/{id}", account1.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(account1.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }
    @Test
    @Transactional
    public void getNonExistingAccount1() throws Exception {
        // Get the account1
        restAccount1MockMvc.perform(get("/api/account-1-s/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccount1() throws Exception {
        // Initialize the database
        account1Service.save(account1);

        int databaseSizeBeforeUpdate = account1Repository.findAll().size();

        // Update the account1
        Account1 updatedAccount1 = account1Repository.findById(account1.getId()).get();
        // Disconnect from session so that the updates on updatedAccount1 are not directly saved in db
        em.detach(updatedAccount1);
        updatedAccount1
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD);

        restAccount1MockMvc.perform(put("/api/account-1-s")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAccount1)))
            .andExpect(status().isOk());

        // Validate the Account1 in the database
        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeUpdate);
        Account1 testAccount1 = account1List.get(account1List.size() - 1);
        assertThat(testAccount1.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testAccount1.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingAccount1() throws Exception {
        int databaseSizeBeforeUpdate = account1Repository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccount1MockMvc.perform(put("/api/account-1-s")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(account1)))
            .andExpect(status().isBadRequest());

        // Validate the Account1 in the database
        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAccount1() throws Exception {
        // Initialize the database
        account1Service.save(account1);

        int databaseSizeBeforeDelete = account1Repository.findAll().size();

        // Delete the account1
        restAccount1MockMvc.perform(delete("/api/account-1-s/{id}", account1.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Account1> account1List = account1Repository.findAll();
        assertThat(account1List).hasSize(databaseSizeBeforeDelete - 1);
    }
}
