package com.team5.monolith.web.rest;

import com.team5.monolith.ShoppingApp;
import com.team5.monolith.domain.FullName;
import com.team5.monolith.repository.FullNameRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FullNameResource} REST controller.
 */
@SpringBootTest(classes = ShoppingApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FullNameResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MID_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MID_NAME = "BBBBBBBBBB";

    @Autowired
    private FullNameRepository fullNameRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFullNameMockMvc;

    private FullName fullName;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FullName createEntity(EntityManager em) {
        FullName fullName = new FullName()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .midName(DEFAULT_MID_NAME);
        return fullName;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FullName createUpdatedEntity(EntityManager em) {
        FullName fullName = new FullName()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .midName(UPDATED_MID_NAME);
        return fullName;
    }

    @BeforeEach
    public void initTest() {
        fullName = createEntity(em);
    }

    @Test
    @Transactional
    public void createFullName() throws Exception {
        int databaseSizeBeforeCreate = fullNameRepository.findAll().size();
        // Create the FullName
        restFullNameMockMvc.perform(post("/api/full-names")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fullName)))
            .andExpect(status().isCreated());

        // Validate the FullName in the database
        List<FullName> fullNameList = fullNameRepository.findAll();
        assertThat(fullNameList).hasSize(databaseSizeBeforeCreate + 1);
        FullName testFullName = fullNameList.get(fullNameList.size() - 1);
        assertThat(testFullName.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testFullName.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testFullName.getMidName()).isEqualTo(DEFAULT_MID_NAME);
    }

    @Test
    @Transactional
    public void createFullNameWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fullNameRepository.findAll().size();

        // Create the FullName with an existing ID
        fullName.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFullNameMockMvc.perform(post("/api/full-names")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fullName)))
            .andExpect(status().isBadRequest());

        // Validate the FullName in the database
        List<FullName> fullNameList = fullNameRepository.findAll();
        assertThat(fullNameList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFullNames() throws Exception {
        // Initialize the database
        fullNameRepository.saveAndFlush(fullName);

        // Get all the fullNameList
        restFullNameMockMvc.perform(get("/api/full-names?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fullName.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].midName").value(hasItem(DEFAULT_MID_NAME)));
    }
    
    @Test
    @Transactional
    public void getFullName() throws Exception {
        // Initialize the database
        fullNameRepository.saveAndFlush(fullName);

        // Get the fullName
        restFullNameMockMvc.perform(get("/api/full-names/{id}", fullName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fullName.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.midName").value(DEFAULT_MID_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingFullName() throws Exception {
        // Get the fullName
        restFullNameMockMvc.perform(get("/api/full-names/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFullName() throws Exception {
        // Initialize the database
        fullNameRepository.saveAndFlush(fullName);

        int databaseSizeBeforeUpdate = fullNameRepository.findAll().size();

        // Update the fullName
        FullName updatedFullName = fullNameRepository.findById(fullName.getId()).get();
        // Disconnect from session so that the updates on updatedFullName are not directly saved in db
        em.detach(updatedFullName);
        updatedFullName
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .midName(UPDATED_MID_NAME);

        restFullNameMockMvc.perform(put("/api/full-names")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFullName)))
            .andExpect(status().isOk());

        // Validate the FullName in the database
        List<FullName> fullNameList = fullNameRepository.findAll();
        assertThat(fullNameList).hasSize(databaseSizeBeforeUpdate);
        FullName testFullName = fullNameList.get(fullNameList.size() - 1);
        assertThat(testFullName.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testFullName.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testFullName.getMidName()).isEqualTo(UPDATED_MID_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingFullName() throws Exception {
        int databaseSizeBeforeUpdate = fullNameRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFullNameMockMvc.perform(put("/api/full-names")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fullName)))
            .andExpect(status().isBadRequest());

        // Validate the FullName in the database
        List<FullName> fullNameList = fullNameRepository.findAll();
        assertThat(fullNameList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFullName() throws Exception {
        // Initialize the database
        fullNameRepository.saveAndFlush(fullName);

        int databaseSizeBeforeDelete = fullNameRepository.findAll().size();

        // Delete the fullName
        restFullNameMockMvc.perform(delete("/api/full-names/{id}", fullName.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FullName> fullNameList = fullNameRepository.findAll();
        assertThat(fullNameList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
