import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingTestModule } from '../../../test.module';
import { Account1UpdateComponent } from 'app/entities/account-1/account-1-update.component';
import { Account1Service } from 'app/entities/account-1/account-1.service';
import { Account1 } from 'app/shared/model/account-1.model';

describe('Component Tests', () => {
  describe('Account1 Management Update Component', () => {
    let comp: Account1UpdateComponent;
    let fixture: ComponentFixture<Account1UpdateComponent>;
    let service: Account1Service;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingTestModule],
        declarations: [Account1UpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(Account1UpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(Account1UpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Account1Service);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Account1(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Account1();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
