import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingTestModule } from '../../../test.module';
import { Account1Component } from 'app/entities/account-1/account-1.component';
import { Account1Service } from 'app/entities/account-1/account-1.service';
import { Account1 } from 'app/shared/model/account-1.model';

describe('Component Tests', () => {
  describe('Account1 Management Component', () => {
    let comp: Account1Component;
    let fixture: ComponentFixture<Account1Component>;
    let service: Account1Service;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingTestModule],
        declarations: [Account1Component],
      })
        .overrideTemplate(Account1Component, '')
        .compileComponents();

      fixture = TestBed.createComponent(Account1Component);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Account1Service);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Account1(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.account1s && comp.account1s[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
