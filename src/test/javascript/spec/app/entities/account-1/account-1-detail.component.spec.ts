import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingTestModule } from '../../../test.module';
import { Account1DetailComponent } from 'app/entities/account-1/account-1-detail.component';
import { Account1 } from 'app/shared/model/account-1.model';

describe('Component Tests', () => {
  describe('Account1 Management Detail Component', () => {
    let comp: Account1DetailComponent;
    let fixture: ComponentFixture<Account1DetailComponent>;
    const route = ({ data: of({ account1: new Account1(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingTestModule],
        declarations: [Account1DetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(Account1DetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Account1DetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load account1 on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.account1).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
