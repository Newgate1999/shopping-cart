import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingTestModule } from '../../../test.module';
import { FullNameComponent } from 'app/entities/full-name/full-name.component';
import { FullNameService } from 'app/entities/full-name/full-name.service';
import { FullName } from 'app/shared/model/full-name.model';

describe('Component Tests', () => {
  describe('FullName Management Component', () => {
    let comp: FullNameComponent;
    let fixture: ComponentFixture<FullNameComponent>;
    let service: FullNameService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingTestModule],
        declarations: [FullNameComponent],
      })
        .overrideTemplate(FullNameComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FullNameComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FullNameService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FullName(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fullNames && comp.fullNames[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
