import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingTestModule } from '../../../test.module';
import { FullNameDetailComponent } from 'app/entities/full-name/full-name-detail.component';
import { FullName } from 'app/shared/model/full-name.model';

describe('Component Tests', () => {
  describe('FullName Management Detail Component', () => {
    let comp: FullNameDetailComponent;
    let fixture: ComponentFixture<FullNameDetailComponent>;
    const route = ({ data: of({ fullName: new FullName(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingTestModule],
        declarations: [FullNameDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FullNameDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FullNameDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fullName on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fullName).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
