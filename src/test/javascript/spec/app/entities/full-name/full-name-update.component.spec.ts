import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingTestModule } from '../../../test.module';
import { FullNameUpdateComponent } from 'app/entities/full-name/full-name-update.component';
import { FullNameService } from 'app/entities/full-name/full-name.service';
import { FullName } from 'app/shared/model/full-name.model';

describe('Component Tests', () => {
  describe('FullName Management Update Component', () => {
    let comp: FullNameUpdateComponent;
    let fixture: ComponentFixture<FullNameUpdateComponent>;
    let service: FullNameService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingTestModule],
        declarations: [FullNameUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FullNameUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FullNameUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FullNameService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FullName(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FullName();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
